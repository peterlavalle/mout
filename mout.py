"""
main body/script of mout. don't commit this to your projects - let the laucnh script check it out as Spock intended
"""

def main(*ext):

	if 0 == len(ext):
		raise Exception("modify your project's MOUT to give some extensions")

	setup_bat()

	src = list_source()
	make_doc(src, *ext)

def setup_bat():
	##
	# create the/a .bat file and make the thing executable
	import os
	if not os.path.isfile('mout.bat'):

		# pandoc - do it here
		os.system('npm install --silent pandoc')

		# get permissions on posix
		import stat
		st = os.stat('mout')
		os.chmod('mout', st.st_mode | stat.S_IEXEC)

		# create a bat file on win32
		with open('mout.bat', 'w') as bat:
			bat.write('@ECHO OFF\n')
			bat.write('\n')
			bat.write('python mout %*\n')
	
		# i guess that's that
		print('mout setup')

def list_source():
	##
	# build the src
	import os
	src = os.listdir('src/')
	src = filter(lambda src: src.endswith('.md'), src)
	src = filter(lambda src: not src.startswith('_'), src)
	src = map(lambda src: ' src/' + src, src)
	src = list(src)
	src.sort()

	return src


def make_doc(src, *ext):
	##
	# make a word doc
	import os
	import functools
	import operator

	os.makedirs('target/', exist_ok = True)
	os.makedirs('target/doc/', exist_ok = True)

	for suffix in ext:
		os.system(
			functools.reduce(operator.add, src, 'pandoc -o target/doc/doc.' + suffix)
		)



